package com.pongplayer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PongplayerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PongplayerApplication.class, args);
	}

}

