package com.pongplayer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
  
@Controller
public class MainController {
  
    @RequestMapping("/welcome")
    public String welcome() {
        return "index";
    }
    @RequestMapping("/ratingcalculator")
    public String calculate() {
        return "calculator.html";
    }
    @RequestMapping("/test")
    public String test() {
        return "test.html";
    }
    @RequestMapping("/admin")
    public String admin() {
        return "admin.html";
    }
}