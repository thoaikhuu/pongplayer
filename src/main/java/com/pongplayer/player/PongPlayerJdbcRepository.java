package com.pongplayer.player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class PongPlayerJdbcRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    class PongPlayerRowMapper implements RowMapper < PongPlayer > {
        @Override
        public PongPlayer mapRow(ResultSet rs, int rowNum) throws SQLException {
        	PongPlayer player = new PongPlayer();
        	player.setId(rs.getLong("id"));
        	player.setName(rs.getString("name"));
        	player.setRating(rs.getInt("rating"));
            return player;
        }
    }

    public List < PongPlayer > findAll() {
        return jdbcTemplate.query("select * from player", new PongPlayerRowMapper());
    }
    public List < PongPlayer > findAllByRating() {
        return jdbcTemplate.query("select * from player order by rating ASC", new PongPlayerRowMapper());
    }
    public PongPlayer findById(long id) {
        return jdbcTemplate.queryForObject("select * from player where id=?", new Object[] {
                id
            },

            new BeanPropertyRowMapper < PongPlayer > (PongPlayer.class));
    }

    public int deleteById(long id) {
        return jdbcTemplate.update("delete from player where id=?", new Object[] {
            id
        });
    }

    public int insert(PongPlayer player) {
        return jdbcTemplate.update("insert into student (id, name, rating) " + "values(?,  ?, ?)",
            new Object[] {
                player.getId(), player.getName(), player.getRating()
            });

    }
    
    public int updatePrevRating(PongPlayer player) {
        return jdbcTemplate.update("update pongplayer " + " set name = ?, prevrating = ? " + " where id = ?",
           new Object[] {
                player.getName(), player.getRating(), player.getId()
            });
    }

    public int update(PongPlayer player) {
    	updatePrevRating(player);
        return jdbcTemplate.update("update pongplayer " + " set name = ?, rating = ? " + " where id = ?",
           new Object[] {
                player.getName(), player.getRating(), player.getId()
            });
    }

}
