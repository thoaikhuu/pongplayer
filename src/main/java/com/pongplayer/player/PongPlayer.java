package com.pongplayer.player;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "player")
public class PongPlayer {
	
	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
    private Long id;
	@Column(name = "name", nullable = false)
    private String name;
	@Column(name = "prevrating", nullable = false)
    private int prevrating;
	@Column(name = "rating", nullable = false)
    private int rating;
	@Column(name = "email", nullable = false)
    private String email;
	@Column(name = "phone", nullable = false)
    private String phone;
	@Column(name = "notes", nullable = true)
    private String notes;

    public PongPlayer() {
        super();
    }

    public PongPlayer(Long id, String name, int rating,int prevrating, String email, String phone, String notes) {
        super();
        this.id = id;
        this.name = name;
        this.prevrating = prevrating;
        this.rating = rating;
        this.email = email;
        this.phone = phone;
        this.notes = notes;
    }

    public PongPlayer(String name, int prevrating, int rating, String email, String phone, String notes) {
        super();
        this.name = name;
        this.prevrating = prevrating;
        this.rating = rating;
        this.email = email;
        this.phone = phone;
        this.notes = notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getPrevrating() {
		return prevrating;
	}

	public void setPrevrating(int prevrating) {
		this.prevrating = prevrating;
	}

    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}


    @Override
    public String toString() {
        return String.format("PongPlayer [id=%s, name=%s, rating=%s]", id, name, rating);
    }

}
