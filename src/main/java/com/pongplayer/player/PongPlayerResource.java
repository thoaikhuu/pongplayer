package com.pongplayer.player;


import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;



@RestController
public class  PongPlayerResource {

		@Autowired
		private  PongPlayerRepository repository;
		
		private  PongPlayerJdbcRepository repository1;
		
		public PongPlayerResource(PongPlayerRepository repository) {
	        this.repository = repository;
	    }
		
		

		@GetMapping("/players")
		
		public List<PongPlayer> retrieveAllPlayers() {
		
			return repository.findAll(new Sort(Sort.Direction.DESC, "rating"));
		}

		
		@GetMapping("/players/{id}")
	    public ResponseEntity<PongPlayer> getPlayerById(@PathVariable(value = "id") Long id)

	        throws ResourceNotFoundException {

	        PongPlayer pplayer = repository.findById(id)

	          .orElseThrow(() -> new ResourceNotFoundException("player not found for this id :: " + id));

	        return ResponseEntity.ok().body(pplayer);

	    }
		
		@DeleteMapping("/players/{id}")
		public void deletePlayer(@PathVariable long id) {
			repository.deleteById(id);
			
		}
		
		
		
		@PostMapping("/players")
		public void addPlayer(@RequestBody PongPlayer player) {
			repository.save(player);
			 System.out.print("Player added");
		}
		
		@PutMapping("/players/{id}")
		/*
		public void updatePlayer(@RequestBody PongPlayer playerdetails, @PathVariable long id) {
			
			PongPlayer pplayer = repository.
			
			pplayer.setId(playerdetails.getId());
	    	pplayer.setName(playerdetails.getName());
	    	pplayer.setRating(playerdetails.getRating());
	    	repository.save(pplayer);
			
		}
		*/
	    public ResponseEntity<PongPlayer> updatePlayer(@PathVariable(value = "id") Long id,

	            @Valid @RequestBody PongPlayer playerdetails) throws ResourceNotFoundException {

	         PongPlayer pplayer = repository.findById(id)

	           .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + id));

	            pplayer.setId(playerdetails.getId());
		    	pplayer.setName(playerdetails.getName());
		    	pplayer.setPrevrating(playerdetails.getPrevrating());
		    	pplayer.setRating(playerdetails.getRating());

	           final PongPlayer updatedPlayer = repository.save(pplayer);

	           return ResponseEntity.ok(updatedPlayer);

	       }
	   
		
	}


