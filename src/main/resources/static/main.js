var app = angular.module("PongPlayerManagement", []);
 
// Controller Part
app.controller("PongPlayerController", function($scope, $http, $window) {
 
 
    $scope.players = [];
    $scope.players1 = [];
    $scope.players2 = [];
    
    $scope.playerForm = {
        id: 1,
        name: "",
        prevrating: 0,
        rating: 0,
        email: "",
        phone: "",
        notes: ""
    };
    $scope.player1Details = {
            id: 0,
            name: "",
            prevrating: 0,
            rating: 0,
            email: "",
            phone: "",
            notes: ""
        };
    $scope.player2Details = {
            id: 0,
            name: "",
            prevrating: 0,
            rating: 0,
            email: "",
            phone: "",
            notes: ""
        };
 
    // Now load the data from server
    _refreshPlayerData();
 
    // HTTP POST/PUT methods for add/edit employee  
    // Call: http://localhost:8080/employee
    $scope.submitPlayer = function() {
 
        var method = "";
        var url = "";
 
        if ($scope.playerForm.id == -1) {
            method = "POST";
            url = '/players';
            console.log("Post " + url);
        } else {
            method = "PUT";
            url = '/players/' + $scope.playerForm.id;
            console.log("PUT " + $scope.playerForm.id + " " + url);
        }
 
        $http({
            method: method,
            url: url,
            data: angular.toJson($scope.playerForm),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(_success, _error);
    };
 
    $scope.createPlayer = function() {
        _clearFormData();
    }
 
    // HTTP DELETE- delete employee by Id
    // Call: http://localhost:8080/employee/{empId}
    $scope.deletePlayer = function(player) {
        $http({
            method: 'DELETE',
            url: '/players/' + player.id
        }).then(_success, _error);
        console.log("DELETE");
    };
 
    // In case of edit
    $scope.editPlayer = function(player) {
        $scope.playerForm.id = player.id;
        $scope.playerForm.name = player.name;
        $scope.playerForm.prevrating = player.rating;
        $scope.playerForm.rating = player.rating;
        $scope.playerForm.email = player.email;
        $scope.playerForm.phone = player.phone;
        $scope.playerForm.notes = player.notes;
    };
    
    $scope.calculateRating = function (player1, player2){
    	
    	if  ($scope.player1Form.id == $scope.player2Form.id)
   		{
    		$window.alert("Oops, You pick the same pong player");
    		
   		}
    	else{
    		angular.forEach($scope.players, function(value, key){
    	        if(value.id == $scope.player1Form.id)
    	        	{
    	        		$scope.player1Details.id = value.id;
    	        		$scope.player1Details.name = value.name;
    	        		$scope.player1Details.rating = value.prevrating;
    	        		$scope.player1Details.rating = value.rating;
    	        		$scope.player1Details.email = value.email;
    	                $scope.player1Details.phone = value.phone;
    	                $scope.player1Details.notes = value.notes;
    	        	}
    	          
    	        if(value.id == $scope.player2Form.id)
    	       	{
    	       		$scope.player2Details.id = value.id;
    	       		$scope.player2Details.name = value.name;
    	       		$scope.player2Details.rating = value.prevrating;
    	       		$scope.player2Details.rating = value.rating;
    	       		$scope.player2Details.email = value.email;
	                $scope.player2Details.phone = value.phone;
	                $scope.player2Details.notes = value.notes;
    	       	}
              
         });
       	
    		//console.log($scope.player1Details.id + " - " + $scope.player1Details.name + " - " + $scope.player1Details.rating);
    		//console.log($scope.player2Details.id + " - " + $scope.player2Details.name + " - " + $scope.player2Details.rating);
    		
    	   
    	}
    	
   }
 
    // Private Method  
    // HTTP GET- get all employees collection
    // Call: http://localhost:8080/employees
    function _refreshPlayerData() {
        $http({
            method: 'GET',
            url: '/players'
        }).then(
            function(res) { // success
                $scope.players = res.data;
                $scope.players1 = res.data;
                $scope.players2 = res.data;
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }
 
    function _success(res) {
        _refreshPlayerData();
        _clearFormData();
    }
 
    function _error(res) {
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        alert("Error: " + status + ":" + data);
    }
 
    // Clear the form
    function _clearFormData() {
        $scope.playerForm.id = -1;
        $scope.playerForm.name = "";
        $scope.playerForm.prevrating = 0;
        $scope.playerForm.rating = 0;
        $scope.playerForm.email = "";
        $scope.playerForm.phone = "";
        $scope.playerForm.notes = "";
    };
    
    
    
});