 function calculate()
		  {
		    with (document.frm)
		    {
		      // REQUIRED
		      if (begrating.value.length < 3 || isNaN(begrating.value))
		      {
		        alert("Invalid beginning rating."); begrating.select(); return false;
		      }
		      if (otherrating1.value.length < 3 || isNaN(otherrating1.value))
		      {
		        alert("Invalid rating for Player 1."); otherrating1.select(); return false;
		      }
		      if (result1.selectedIndex == 0)
		      {
		        alert("You did not indicate whether you won or lost to Player 1."); result1.focus(); return false;
		      }
		      
		      // OPTIONAL
		      var opponents = new Array(null, true, false, false, false, false, false, false)
		      var textboxes = new Array(null, otherrating1, otherrating2, otherrating3, otherrating4, otherrating5, otherrating6, otherrating7)
		      var selectboxes = new Array(null, result1, result2, result3, result4, result5, result6, result7)
		      var enums = new Array(null, null, "second", "third", "fourth", "fifth", "sixth", "seventh")
		      for (var i = 2; i <= 7; i++)
		      {
		        if ((textboxes[i].value.length > 2 && selectboxes[i].selectedIndex == 0) || (textboxes[i].value.length < 3 && selectboxes[i].selectedIndex > 0))
		        {
		          alert("Invalid information for Player" + i + ". You may have entered a valid rating without indicating the result of the match or you indicated a result without a valid rating. Player " + i + " is optional. If you did not play a " + enums[i] + " match, clear the fields from this row."); selectboxes[i].focus(); return false;
		        }
		        else
		        {
		          if (textboxes[i].value.length > 2 && selectboxes[i].selectedIndex > 0) opponents[i] = true;
		        }
		      }
		      
		      // CALCULATE
		      var endrating = 0;
		      
		      for (i = 1; i <=7; i++)
		      {
		        if (opponents[i])
		        {
		          favorite = false; winner = false;
		          if (parseInt(begrating.value) >= parseInt(textboxes[i].value)) favorite = true
		          if (selectboxes[i].selectedIndex == 1) winner = true
		          difference = Math.abs(begrating.value - textboxes[i].value)
		          endrating += matchcalculate(favorite, winner, difference)
		        }
		      }
		      result.value = parseInt(begrating.value) + endrating
		    }
		  }
		  function matchcalculate(favorite, winner, difference)
		  {
		    var aindex = indexFromDiff(difference)
		    var points1 = new Array(8, 7, 6, 5, 4, 3, 2, 2, 1, 0, 0);
		    var points2 = new Array(-8, -10, -13, -16, -20, -25, -30, -35, -40, -45, -50);
		    var points3 = new Array(-8, -7, -6, -5, -4, -3, -2, -2, -1, 0, 0);
		    var points4 = new Array(8, 10, 13, 16, 20, 25, 30, 35, 40, 45, 50);
		    if (favorite && winner) return points1[aindex];
		    if (favorite && !winner) return points2[aindex];
		    if (!favorite && !winner) return points3[aindex];
		    if (!favorite && winner) return points4[aindex];
		  }
		  function indexFromDiff(difference)
		  {
		    if (difference <= 12) return 0;
		    if (difference >=13 && difference <=37) return 1;
		    if (difference >=38 && difference <=62) return 2;
		    if (difference >=63 && difference <=87) return 3;
		    if (difference >=88 && difference <=112) return 4;
		    if (difference >=113 && difference <=137) return 5;
		    if (difference >=138 && difference <=162) return 6;
		    if (difference >=163 && difference <=187) return 7;
		    if (difference >=188 && difference <=212) return 8;
		    if (difference >=213 && difference <=237) return 9;
		    if (difference >=238) return 10;
		  }