create table player
(
   id integer not null,
   name varchar(255) not null,
   prevrating integer not null,
   rating integer not null,
   email varchar(255) not null,
   phone varchar(255) not null,
   notes varchar(1000),
   primary key(id)
);